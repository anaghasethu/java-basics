package com.npci.application;

public class Palindrome {
	public static void main(String[] args) {
		String string1 = "Malayalam";
		string1.toLowerCase();
		int i=0, j=string1.length()-1;
		boolean flag = false;
		while(i<j) {
			if(string1.charAt(i) != string1.charAt(j) ) {
				flag = false;
			}
			else {
				flag = true;
			}
			i++;
			j--;
		}
		
		if(flag == true) {
			System.out.println("Palindrome");
		}
		else {
			System.out.println("Not Palindrome");
		}
		
		
		
	}
}
