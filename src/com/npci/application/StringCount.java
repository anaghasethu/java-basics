package com.npci.application;

public class StringCount {
	public static void main(String[] args) {
		String string = "Hello world!";
		int c = string.length();
		
		for(int i = 0 ; i<string.length();i++) {
			if(string.charAt(i) == ' ') {
				c--;
			}
		}
		System.out.println("Total number of characters is " +c);
	}
}
