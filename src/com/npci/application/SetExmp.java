package com.npci.application;

import java.util.HashSet;
import java.util.Set;

public class SetExmp {
	public static void main(String[] args) {
		Set<Integer> s = new HashSet<Integer>();
		s.add(30);
		s.add(10);
		s.add(80);
		s.add(20);
		System.out.println(s);;
		s.remove(10);
		System.out.println(s);
		s.clear();
		System.out.println(s);
		
		Set<String> st = new HashSet<String>();
		st.add("Anagha");
		st.add("Jeevan");
		st.add("Vrinda");
		System.out.println(st);
		st.remove("Anagha");
		System.out.println(st);
	}
}
