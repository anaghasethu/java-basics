package com.npci.application;

public class PalindromeCheck {
	public static void main(String[] args) {
		String str = "hello";
		isPalindrome(str);
		
	}
	
	public static void isPalindrome(String str) {
		int i=0, j=str.length()-1;
		boolean flag = false;
		while(i<j) {
			if(str.charAt(i) != str.charAt(j) ) {
				flag = false;
			}
			else {
				flag = true;
			}
			i++;
			j--;
		}
		
		if(flag == true) {
			System.out.println("Yes Palindrome");
		}
		else {
			System.out.println("Not Palindrome");
		}
	}
}
