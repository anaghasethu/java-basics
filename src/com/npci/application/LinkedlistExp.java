package com.npci.application;

import java.util.LinkedList;

public class LinkedlistExp {
	public static void main(String[] args) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		list.add(10);
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(3);
		list.add(5);
		list.add(6);
		list.add(7);
		System.out.println(list);
		list.add(3, 4);
		System.out.println(list);
		list.remove(4);
		System.out.println(list);
		list.removeFirst();
		list.removeLast();
		list.removeLast();
		
		System.out.println(list);
	}
}
