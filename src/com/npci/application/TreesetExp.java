package com.npci.application;

import java.util.TreeSet;

public class TreesetExp {
	public static void main(String[] args) {
		TreeSet<Integer> ts = new TreeSet<Integer>();
		ts.add(20);
		ts.add(11);
		ts.add(2);
		ts.add(3);
		System.out.println(ts);
		ts.remove(0);
		ts.add(4);
		System.out.println(ts);
		ts.add(5);
		System.out.println(ts);
	}
}
