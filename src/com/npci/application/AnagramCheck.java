package com.npci.application;

import java.util.Arrays;

public class AnagramCheck {
	public static void main(String[] args) {
		String s1 = "Hello";
		String s2 = "ohello";
		String string1 = s1.toLowerCase();
		String string2 = s2.toLowerCase();
		if(string1.length() == string2.length()) {
			char[] str1 = string1.toCharArray();
			char[] str2 = string2.toCharArray();
 			
			Arrays.sort(str1);
			Arrays.sort(str2);
			
			if(Arrays.equals(str1, str2)) {
				System.out.println("Anagram");
			}
			else {
				System.out.println("Not anagram");
			}
		}
		else {
			System.out.println("Not anagram");
		}
		
	}
}
