package com.npci.application;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ArrayListExp {
	public static void main(String[] args) {
		ArrayList a = new ArrayList();
		a.add("12");
		a.add("A");
		a.add("Anagha");
		System.out.println(a);
		a.add(2,"76577");
		System.out.println(a);
		ArrayList<String> a1 = new ArrayList<String>();
		a1.add("anagha");
		a1.add("sethu");
		for(String s:a1) {
			System.out.println(s);
		}
		List b = new ArrayList();
		b.add("80");
		b.add("20");
		b.add("50");
		
		System.out.println(b);
		
		Collections.sort(b);
		System.out.println(b);
		
	}
}
