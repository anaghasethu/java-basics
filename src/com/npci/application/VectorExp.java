package com.npci.application;

import java.util.Vector;

public class VectorExp {
	public static void main(String[] args) {
		Vector<Integer> v = new Vector<Integer>();
		v.add(10);
		v.add(1);
		v.add(2);
		v.add(3);
		System.out.println(v);
		v.remove(0);
		v.add(0, 0);
		System.out.println(v);
	}
}
