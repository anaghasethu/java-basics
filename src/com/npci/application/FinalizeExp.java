package com.npci.application;

public class FinalizeExp {
	public static void main(String[] args) {
		FinalizeExp obj = new FinalizeExp();
		System.out.println("The code is "+obj.hashCode());
		
		obj = null;
		System.gc();
		System.out.println("End");
	}
	
	protected void finalize() {
		System.out.println("Called the method");
	}
}
