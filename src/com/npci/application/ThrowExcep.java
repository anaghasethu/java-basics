package com.npci.application;

public class ThrowExcep {
	public static void main(String[] args) {
		ThrowExcep obj = new ThrowExcep();
		obj.area(-5);
	}
	
	public static void area(int num) {
		if(num<1) {
			throw new ArithmeticException("Area cannot be negative");
		}
		else {
			System.out.println("Area is: " +num*num);
		}
	}
}
