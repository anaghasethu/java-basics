package com.npci.application;

public class CaseChange {
	public static void main(String[] args) {
		String string = "HelloWorld";
		StringBuffer newstring = new StringBuffer();
		int length = string.length();
		
		for(int i=0;i<length;i++) {
			if(Character.isLowerCase(string.charAt(i))) {
				newstring.setCharAt(i, Character.toUpperCase(string.charAt(i)));
		}
			else {
				newstring.setCharAt(i, Character.toLowerCase(string.charAt(i)));
			}
	}
		System.out.println(newstring);    
	}
}