package com.npci.application;

public class StringReverse {
	public static void main(String[] args) {
		String string = "Helloworld";
		String newstring = "";
		char ch;
		int length = string.length();
		
		for(int i=0;i<length;i++) {
			ch = string.charAt(i);
			newstring = ch + newstring;
		}
		System.out.println(newstring);
	}
}
