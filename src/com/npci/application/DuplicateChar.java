package com.npci.application;

public class DuplicateChar {
	public static void main(String[] args) {
		String string1 = "Hello my name is anagha"; 
        int count = 1; 		
		String string2 = string1.toLowerCase();
		char[] string = string2.toCharArray();
		
		int length = string1.length();
		
		for(int i = 0; i <length; i++) { 
            for(int j = i+1; j <length; j++) {  
                if(string[i] == string[j] && string[i] != ' ') {  
                    count++;  
                    string[j] = '0';  
                }  
            } 
            if(count > 1 && string[i] != '0')  
                System.out.println(string[i]);  
        }    
	}
}
