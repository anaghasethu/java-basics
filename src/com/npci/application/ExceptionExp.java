package com.npci.application;


public class ExceptionExp {
	public static void main(String[] args) {
		try {
			int a=10,b=0;
			int c= a/b;
			System.out.println(c);
		}
		catch (ArithmeticException ae){
			System.out.println("Arithmetic exception");
			System.out.println(ae);
		}
		catch (IndexOutOfBoundsException ie) {
			System.out.println(ie);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
}
