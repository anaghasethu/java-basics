package com.npci.application;

public class TestClass {
	public static void main(String[] args) {
		TestClass tc = new TestClass();
		int sum = tc.add(10,20);
		System.out.println(sum);
		float sum1 = tc.add(20.2f, 30.3f);
		System.out.println(sum1);
		
	}
	public int add(int a, int b) {
		int c = a+b;
		return c;
	}
	public float add(float a, float b) {
		float c = a+b;
		return c;
	}
}

