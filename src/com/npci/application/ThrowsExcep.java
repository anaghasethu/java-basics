package com.npci.application;

public class ThrowsExcep {
	public static void main(String[] args) {
		ThrowsExcep obj =  new ThrowsExcep();
		try {
			System.out.println(obj.divide(4,1));
		}
		catch (ArithmeticException e) {
			// TODO: handle exception
			System.out.println("Cannot be divided by zero");
		}
		finally {
			System.out.println("Thank you!!");
		}
	}
	public static int divide(int a, int b) throws ArithmeticException {
		int c = a/b;
		return c;
	}
}
