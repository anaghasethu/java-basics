package oops;



public abstract class MyClass {
	//Create a complete method
	public void calculator() {
		System.out.println("Calculating...");
	}
	//abstract method
	abstract public void launchRocket(int num) ;
}

//abstract class cannot be instantiated

class Start{
	public static void main(String[] args) {
		MyChild mychild = new MyChild();
		mychild.calculator();
		mychild.launchRocket(5);

	
	}
}