package oops;

//parent class
public class Parent {
	//overridden method
	protected A m1() {
		System.out.println("I am m1 method of parent!");
		return new A();
	}
}
//child class
class child extends Parent{
	//overriding method
	//co-varient types allowed
	public B m1() { //same type of A as B extends A
		System.out.println("I am m1 of child!");
		return new B();
	}
	
}

class A{ }

class B extends A { }