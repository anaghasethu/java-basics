package oops;

class Student {
	int studentId;
	String studentName;
	String studentCity;
	
	public Student() {
		System.out.println("Non param");
	}
	
	public Student(int studentId, String studentName, String studentCity) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentCity = studentCity;
		System.out.println("done!");
	}
	
	public void study() {
		System.out.println(studentName+ " is studying");
		System.out.println(this.studentCity);
	}
	
//	public void study(String name, String city) {
//		System.out.println(studentName+ " is studying");
//	}
//	public int study(String name, String city) {
//		System.out.println(studentName+ " is studying");
//	}
//	public void study1(String name, String city) {
//		System.out.println(studentName+ " is studying");
//	}
//	
	public void showFullDetails() {
		System.out.println("My name is " +studentName);
		System.out.println("My id is " + studentId);
		System.out.println("My city is "+ studentCity);
	}
}
