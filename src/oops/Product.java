package oops;

public class Product {
	int product_id;
	String prodName;
	String prodBrand;
	
	public Product() {
		this(99);
		System.out.println("Created!");
		
	}
	
	public Product(int id, String name, String brand) {
		product_id = id;
		prodName = name;
		prodBrand = brand;
		System.out.println("Created!");
	}
	public Product(int i) {
		// TODO Auto-generated constructor stub
		System.out.println("Non param int: " +i);
	}

	public void getProduct() {
		System.out.println("Product id: "+product_id);
		System.out.println("Product name: " +prodName);
		System.out.println("Product brand: " +prodBrand);
	}
	public void productname() {
		System.out.println("product is " +this.prodName);
	}
}
